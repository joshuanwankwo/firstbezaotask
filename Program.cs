﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using firstBezaoAssighment.Services;

namespace firstBezaoAssighment
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name");
            var acceptName = Console.ReadLine();
            var Name = UserDetails.GetName(acceptName);

            Console.WriteLine("..........................");

            Console.WriteLine("Please enter your username");
            var acceptUserName = Console.ReadLine();
            var username = UserDetails.GetUserName(acceptUserName);

            Console.WriteLine("...............................");

            Console.WriteLine("Please enter your year of birth");
            var acceptAge = int.Parse(Console.ReadLine());
            var userAge = UserDetails.GetAge(acceptAge);

            Console.WriteLine(".....................................................................................................................................");

            var message = $"Hello {Name}, you chose {username} as your username and from the data you provided we are able to tell that you are {userAge} years old.";
            Console.WriteLine(message);
            Console.WriteLine(".....................");
            Console.WriteLine("Press Enter key to exit");
            Console.ReadLine();
        }

    }
}
